package constants

type LoanStatus int

const (
	Pending  LoanStatus = 1
	Approved LoanStatus = 2
	Declined LoanStatus = 3
)

type LoanInfo int

const (
	New   LoanInfo = 1
	Topup LoanInfo = 2
	HP    LoanInfo = 3
)
