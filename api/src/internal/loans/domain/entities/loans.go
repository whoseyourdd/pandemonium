package entities

import (
	"time"

	"github.com/google/uuid"
	"github.com/uptrace/bun"
	"github.com/whoseyourdd/pandemonium/internal/pkg/constants"
)

type Loans struct {
	bun.BaseModel `bun:"table:loans,alias:l"`

	ID          uuid.UUID            `bun:",pk,type:uuid,default:uuid_generate_v4()"`
	ClientID    int                  `bun:"client_id"`
	Amount      int                  `bun:"amount"`      //jumlah pinjaman
	Duration    int                  `bun:"duration"`    //berapa kali bayar
	Interest    int                  `bun:"interest"`    //bunga
	DueAmount   int                  `bun:"due_amount"`  //kekurangan bayar
	TotalRepay  int                  `bun:"total_repay"` //pinjaman + bunga
	Status      constants.LoanStatus `bun:"status"`      //pending, active, done
	Infos       constants.LoanInfo   `bun:"infos"`       //topup/new/hp
	Description string               `bun:"description"` //deskripsi

	CreatedAt  time.Time `bun:"created_at,default:current_timestamp"`
	UpdatedAt  time.Time `bun:"updated_at,default:current_timestamp"`
	DeletedAt  time.Time `bun:"deleted_at,soft_delete,nullzero"`
	IsFullPaid bool      `bun:"full_paid,default:false"`
	IsActive   bool      `bun:"is_active,default:true"`
	CreatedBy  string    `bun:"created_by"`

	Repayments []*Repayments `bun:"rel:has-many,join:id=loan_id"`
}
