package entities

import (
	"time"

	"github.com/google/uuid"
	"github.com/uptrace/bun"
)

type Repayments struct {
	bun.BaseModel `bun:"table:repayments,alias:r"`

	ID          uuid.UUID `bun:",pk,type:uuid,default:uuid_generate_v4()"`
	LoanID      uuid.UUID `bun:"loan_id,type:uuid"`
	Amount      int       `bun:"amount"`
	Description string    `bun:"description"`
	CreatedAt   time.Time `bun:"created_at,default:current_timestamp"`
	UpdatedAt   time.Time `bun:"updated_at,default:current_timestamp"`
	DeletedAt   time.Time `bun:"deleted_at,soft_delete,nullzero"`
	CreatedBy   string    `bun:"created_by"`
}
