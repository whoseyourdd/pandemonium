package entities

import (
	"time"

	"github.com/uptrace/bun"
)

type Client struct {
	bun.BaseModel `bun:"table:clients,alias:c"`

	ID   int    `bun:"id,pk,autoincrement"`
	Name string `bun:"name,notnull"`
	// Address    string `bun:"address,nullzero"`
	// Phone      string `bun:"phone,nullzero"`
	DivisionID int `bun:"division_id,notnull"`
	BranchID   int `bun:"branch_id,notnull"`
	// IsHaveLoan bool `bun:"have_loan,notnull,default:false"`

	CreatedAt time.Time `bun:"created_at,default:current_timestamp"`
	UpdatedAt time.Time `bun:"updated_at,default:current_timestamp"`
	DeletedAt time.Time `bun:"deleted_at,soft_delete,nullzero"`

	Division *Divisions `bun:"rel:belongs-to,join:division_id=id"`
	Branch   *Branches  `bun:"rel:belongs-to,join:branch_id=id"`
}
