package entities

import (
	"time"

	"github.com/uptrace/bun"
)

type Divisions struct {
	bun.BaseModel `bun:"table:divisions,alias:d"`

	ID   int    `bun:"id,pk,autoincrement"`
	Name string `bun:"name,notnull"`

	CreatedAt time.Time `bun:"created_at,default:current_timestamp"`
	UpdatedAt time.Time `bun:"updated_at,default:current_timestamp"`
	DeletedAt time.Time `bun:"deleted_at,soft_delete,nullzero"`
}
